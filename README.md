
To use Google Sheet API
1. You need to create OAuth 2.0 credential
2. Download the credential in JSON form and add it to websiteRetrieval package
3. In GoogleSheet.java file under websiteRetrieval package match the JSON file
	   name to the string in GoogleSheet.class.getResourceAsStream("ENTER JSON FILE NAME");
4. In the retrieveFeed function, change the spreadsheetId to the id of your Google Sheet.
	   (id is the string that follows after  https://docs.google.com/spreadsheets/d/ )
	   
	   
To start, in Driver.java under aggregator package, fill out the missing string values.
	