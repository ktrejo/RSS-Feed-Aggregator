/**
 * 
 */
package aggregator;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import websiteRetrieval.GoogleSheet;

/**
 * @author Kevin
 *
 */
public class Driver {

	@SuppressWarnings("deprecation")
	public static Date timeStamp = new Date(117, 7, 12);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List<String> rssFeeds = null;
		
		try {
			
			rssFeeds = GoogleSheet.retrieveFeed();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RSS rss = new RSS();
		Email email = new Email();
		// Must fill missing strings
		String to = "";
		String from = "";
		String fromPassword = "";
		
		List<FeedPacket> fpList = rss.processFeeds(rssFeeds, timeStamp);
		
		email.sendEMail(fpList, to, from, fromPassword);
		

	}

}
