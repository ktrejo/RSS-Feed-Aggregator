/**
 * 
 */
package aggregator;

/**
 * @author Kevin
 *
 */
public class FeedPacket {

	private String title;
	private String urlLink;
	private String description;
	private String feedURL;

	public FeedPacket(String title, String urlLink, String description, String baseURL) {

		this.title = title;
		this.urlLink = urlLink;
		this.description = description;
		this.feedURL = baseURL;

	}

	public String getTitle() {
		return title;
	}

	public String getUrlLink() {
		return urlLink;
	}

	public String getDescription() {
		return description;
	}

	public String getFeedURL() {
		return feedURL;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}

	public void setDescription(String text) {
		this.description = text;
	}

	public void setBaseURL(String baseURL) {
		this.feedURL = baseURL;
	}

	@Override
	public String toString() {
		String s = "Title: " + title + "\n" + "Link: " + urlLink + "\n";
		
		if (description.length() != 0) 
			s += "about: " + description + "\n";
		
		return s;

	}

}
