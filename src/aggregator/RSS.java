/**
 * 
 */
package aggregator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.net.URL;
import java.io.IOException;

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

/**
 * @author Kevin Trejo
 *
 */
public class RSS {
	
	/**
	 * Given the list of links and date, processFeeds will visit each https links
	 * and retrieve all the info that is timestamp after and including date.
	 * 
	 * @param links
	 * @param date
	 * @return List of all rss info that is timestamp at date and after.
	 */
	public List<FeedPacket> processFeeds (List<String> links, Date date) {
		// might need to create and object to store info. Might need to update parameter
		
		List<FeedPacket> rssFeeds = new ArrayList<FeedPacket>();
		
		if (links == null)
			return null;
		
		for (String s : links) {
			
			readFeed(rssFeeds, date, s);
			
		}
		
		return rssFeeds;
		
	}
	
	/**
	 * visits rss url and add all new information that occured after and including 
	 * param date to param list
	 * 
	 * @param list
	 * @param date
	 * @param url
	 */
	private void readFeed(List<FeedPacket> list, Date date, String url) {
		
		try {
			
			URL feedUrl = new URL(url);
			
			
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed feed = input.build(new XmlReader(feedUrl));
			
			List<SyndEntry> feedEntries = feed.getEntries();
			Date seDate;
			FeedPacket temp;
			SyndContent descript;
			
			for (SyndEntry se : feedEntries) {
				
				seDate = (se.getUpdatedDate() == null)? se.getPublishedDate() : se.getUpdatedDate();
				
				if (seDate == null)
					continue;
				
				if (seDate.compareTo(date) >= 0) {
					
					temp = new FeedPacket(se.getTitle(), se.getLink(), "", url);
					descript = se.getDescription();
					if (descript != null)
						temp.setDescription(descript.getValue());
					
					list.add(temp);
					
				}
				
			}
			
		} catch (IllegalArgumentException | FeedException | IOException e) {
			
			/*
			 *  Should the method failed to connect to the rss url, only one FeedPacket
			 *  will be created and will indicate that the connection has failed.
			 */
			
			list.add(new FeedPacket("BAD URL or could NOT connect: ", url, "", url));
			
		}
		
	}
		
}
