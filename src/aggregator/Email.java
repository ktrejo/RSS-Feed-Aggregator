/**
 * 
 */
package aggregator;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 * @author Kevin
 *
 */
public class Email {

	/**
	 * Sends an email of rssFeed to to.
	 * 
	 * @param rssFeed
	 * @param to
	 * @param roPassword
	 * 
	 */
	public void sendEMail(List<FeedPacket> rssFeed, String to, String from, String fromPassword) {

		StringBuilder sb = new StringBuilder();

		if (rssFeed == null || rssFeed.size() == 0)
			sb.append("No new news");

		else {
			sb.append("The new news are: \n\n\n\n");

			String currFeed = "";

			for (FeedPacket fp : rssFeed) {

				if (!currFeed.equals(fp.getFeedURL())) {
					currFeed = fp.getFeedURL();
					sb.append("News from: " + currFeed + "\n");
				}

				sb.append(fp.toString() + "\n");

			}
		}

		doSendMail(from, fromPassword, to, "Today's feed", sb.toString());

	}

	/**
	 * update htmlString
	 * 
	 * @param curr
	 * @param add
	 * @return new html formatt
	 */
	public String htmlFormat(String curr, String add) {
		// Use string builder to improve efficiency.

		return null;
	}

	private void doSendMail(final String username, final String password, String to, String subject,
			String email_body) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "587");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", "587");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(email_body);
			Transport.send(message);
			//System.out.println("message sent");
			
		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
		}
	}

}
