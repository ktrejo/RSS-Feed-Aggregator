package test.aggregator;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

//import static org.junit.Assert.*;

import org.junit.Test;

import aggregator.Email;
import aggregator.FeedPacket;
import aggregator.RSS;

public class RSSTest {
	
	@SuppressWarnings("deprecation")
	public static Date timeStamp = new Date(117, 7, 12);
	
	public static List<String> links = new LinkedList<String>();
	
	public static RSS rss = new RSS();

	@Test
	public void url1() {
		
		String rssURL = "https://www.reddit.com/r/UMD/.rss";
		links.add(rssURL);
		printFeeds(rss.processFeeds(links, timeStamp));
		
	}
	
	@Test
	public void url2() {
		
		String rssUrl = "http://rss.nytimes.com/services/xml/rss/nyt/Americas.xml";
		links.add(rssUrl);
		printFeeds(rss.processFeeds(links, timeStamp));
		
	}
	
	@Test
	public void url3() {
		
		List<FeedPacket> fb = rss.processFeeds(links, timeStamp);
		Email mail = new Email();
		String to = "";
		String from = "";
		String fromPassword = "!";
		mail.sendEMail(fb, to, from, fromPassword);
		
	}
	
	private void printFeeds(List<FeedPacket> list) {
		
		String baseURL = "";
		
		for (FeedPacket fp : list) {
			
			if (!baseURL.equals(fp.getFeedURL())) {
				baseURL = fp.getFeedURL();
				System.out.println("Website: " + baseURL);
			}
			
			System.out.println(fp.toString() + "\n");
			
		}
		
	}

}
